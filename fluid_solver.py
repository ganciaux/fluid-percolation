"""
Finite Volume/Finite Difference solver for fluid flow in a non-homogeneous gap field.
It solves the diffusion problem for the pressure field and calculates the flux.
Connectivity is checked, trapped fluid lakes are excluded from simulation.

Author: Vladislav A. Yastrebov, CNRS, MINES Paris, PSL University, Aug 2024
LLM usage: Claude by Anthtropic and GPT4o by OpenAI assisted in the development of this code.
License: BSD 3-Clause
"""

import numpy as np
from scipy.ndimage import label
from scipy.sparse import lil_matrix
# For iterative solver if needed
# from pyamg import smoothed_aggregation_solver
# from scipy.sparse.linalg import spilu, LinearOperator, cg, gmres, bicgstab, spsolve

from pypardiso import spsolve as pypardiso_spsolve

import time
import logging
import gc

logger = logging.getLogger(__name__)


def setup_logging(level=logging.INFO):
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '/FS: %(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)


def set_verbosity(level='info'):
    levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
    }
    logger.setLevel(levels.get(level.lower(), logging.INFO))


"""
Create the sparse matrix for the diffusion problem with non-homogeneous gap field,
properly handling zero or near-zero gap regions.
"""


def create_diffusion_matrix(n, g, penalty):
    N = n * n
    A = lil_matrix((N, N))
    b = np.zeros(N)

    def index(i, j):
        return i * n + j

    dx = 1.0 / (n - 1)

    for i in range(n):
        for j in range(n):
            idx = index(i, j)

            if i == n - 1:  # Right boundary (x = 1)
                if g[i, j] != 0:
                    A[idx, idx] = penalty
                    b[idx] = penalty
                else:
                    A[idx, idx] = 1
                    b[idx] = 0
            elif i == 0:  # Left boundary (x = 0)
                A[idx, idx] = penalty
                b[idx] = 0
            else:
                g_e = 0.5 * (g[i, j]**3 + g[i+1, j]**3)
                g_w = 0.5 * (g[i, j]**3 + g[i-1, j]**3)
                g_n = 0.5 * (g[i, j]**3 + g[i, (j+1) % n]**3)
                g_s = 0.5 * (g[i, j]**3 + g[i, (j-1) % n]**3)
                if g[i+1, j] == 0:
                    g_e = 0
                if g[i-1, j] == 0:
                    g_w = 0
                if g[i, (j+1) % n] == 0:
                    g_n = 0
                if g[i, (j-1) % n] == 0:
                    g_s = 0
                if g[i, j] > 0:
                    A[idx, idx] = -(g_e + g_w + g_n + g_s) / dx
                    if g[i+1, j] > 0:
                        A[idx, index(i+1, j)] = g_e / dx
                    if g[i-1, j] > 0:
                        A[idx, index(i-1, j)] = g_w / dx
                    if g[i, (j+1) % n] > 0:
                        A[idx, index(i, (j+1) % n)] = g_n / dx
                    if g[i, (j-1) % n] > 0:
                        A[idx, index(i, (j-1) % n)] = g_s / dx
                else:
                    A[idx, idx] = 1

    return A, b


def solve_diffusion(n, g, solver):
    A, b = create_diffusion_matrix(n, g, 1e3)
    A = A.tocsr()

    if solver == "direct":
        gc.collect()
        p = pypardiso_spsolve(A, b)
    elif solver == "iterative":
        # AMG preconditioner
        ml = smoothed_aggregation_solver(A, max_coarse=A.shape[0] // 1000)
        M = ml.aspreconditioner(cycle="V")

        # Alternative preconditioner
        # ilu = spilu(A.tocsc(), drop_tol=1e-5)
        # M = LinearOperator(A.shape, ilu.solve)

        p, info = gmres(A, b, M=M, rtol=1e-12, maxiter=6000, restart=30)

        # Alternative solvers
        # p, info = cg(A, b, M=M, rtol=1e-12, maxiter=5000) # Alternative solver #2
        # p, info = bicgstab(A, b, M=M, rtol=1e-22, maxiter=10000) # Alternative solver #3

        if info > 0:
            print(
                f"Convergence to tolerance not achieved in {info} iterations")
        elif info < 0:
            print(f"Illegal input or breakdown: {info}")

    return p.reshape((n, n))


def threshold(matrix, z0):
    return (matrix > z0).astype(int)


def solve_fluid_problem(gaps, solver) -> tuple:
    """
    Solves the fluid problem for a given gap field.

    Parameters
    ----------
    gaps : 2D array
        Gap field.
    solver : str
        Solver to use for the diffusion problem. Can be "direct" or "iterative".

    Returns
    -------
    tuple
        Tuple containing the percolation boolean (0 - no percolation, 1 - percolation), the gap field, the pressure field (scalar 2D array) and the flux field (vector 2D array).
    """
    logger.info("Starting fluid solver.")
    n = gaps.shape[0]
    if n == 0:
        logger.error("Empty gap field.")
        return None

    x = np.linspace(0, 1, n)
    X, Y = np.meshgrid(x, x)

    logger.info("Checking connectivity.")
    # construct connectivity matrix
    binary = threshold(gaps, 0)

    # Select separate regions
    labels, numL = label(binary)

    # Make labels periodic
    for i in range(n):
        if labels[i, 0] > 0:
            color = labels[i, 0]
            opposite = labels[i, -1]
            if opposite > 0:
                labels[labels == opposite] = color

    # Check if there is a percolation
    left_side = labels[0, :]
    right_side = labels[-1, :]
    unique_left_side = np.unique(left_side)
    unique_right_side = np.unique(right_side)
    selected_color = -1

    for i in unique_left_side:
        if i != 0:
            for j in unique_right_side:
                if j != 0 and i == j:
                    selected_color = i
                    break
    if selected_color == -1:
        logger.info("Percolation detected.")
        return 1, gaps, None, None, None, None

    # To get rid of lakes surrounded by contact (trapped fluid)
    gaps = gaps * (labels == selected_color)

    # Calculate area of trapped zones that are not connected to left or right sides
    trapped_labels = set(np.unique(labels)) - set(unique_left_side) - \
        set(unique_right_side) - {0, selected_color}
    trapped_area = np.sum(np.isin(labels, list(trapped_labels)))
    logger.info(f"Trapped area: {trapped_area}")

    # Calculate conductive area (connected region between left and right sides)
    conductive_area = np.sum(labels == selected_color)
    logger.info(f"Conductive area: {conductive_area}")

    logger.info("Solving diffusion problem.")
    try:
        # Solve for pressure
        start_time = time.time()
        p = solve_diffusion(n, gaps, solver)
        logger.info("Fluid solver: CPU time for n = {0:d}: {1:.3f} sec".format(
            n, time.time() - start_time))
    except Exception as e:
        logger.error("Error in fluid solver: ", e)
        return None

    logger.info("Fluid solver finished.")
    logger.info("Calculating flux.")

    # Calculate flux
    dx = 1 / (n - 1)
    dpdx = np.gradient(p, dx, axis=1)
    dpdy = np.gradient(p, dx, axis=0)

    flux = np.zeros((n, n, 2))
    # Could be improved by using the average of the gaps
    flux[:, :, 0] = -gaps**3 * dpdx
    flux[:, :, 1] = -gaps**3 * dpdy
    filtered_flux = flux.copy()
    for i in range(n):
        for j in range(n):
            if gaps[i, j] == 0 or gaps[(i+1) % n, j] == 0 or gaps[i, (j+1) % n] == 0 or gaps[(i-1) % n, j] == 0 or gaps[i, (j-1) % n] == 0:
                filtered_flux[i, j] = np.nan

    logger.info("finished.")

    return 0, gaps, p, filtered_flux, trapped_area, conductive_area
