#!/bin/env bash

# python discretization_study.py --help
python3.10 discretization_study.py --N 128 --Nmax 128 --desired_rms 1. --hurst 0.8 --k0 1 --k1 2 --k2 64 --seed 1 --epsilon 1e-12 --e_star 1 --distribution_factor 5 --compute_flux True --plot_surface True --do_not_use_akantu


