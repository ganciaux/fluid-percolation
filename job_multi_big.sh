#!/bin/bash
#SBATCH --cpus-per-task 1
#SBATCH --nodes 1
#SBATCH --ntasks 72
#SBATCH --qos serial
#SBATCH --partition standard
#SBATCH --time 3-00:00:00

for proc in `seq 1 72`;
do
    canYouDigIt runs launch --nruns 1 --truerun --constraint "run_name = test_4096, n = 4096" --machine_name jed &
    sleep 5
done

wait
