#!/usr/bin/env python3.10
################################################################
import sys
import os
import argparse
import subprocess
import BlackDynamite as BD
import tamaas as tm
import numpy as np
################################################################
import logging
logger = logging.getLogger(__name__)
################################################################
stats = tm.Statistics2D
################################################################

# def computeFluidFlux(trac,disp,surf):
#    import analyse_flux_case
#    temperature,percolated = analyse_flux_case.computeFlux(trac,disp,surf)
#    blocked = (temperature == -1)
#    temperature[blocked] = np.nan
#
#    gaps = disp - surf - disp.max() + surf.max()
#    gaps = analyse_flux_case.renderPBC4FEM(gaps)
#
#    mu = 1.
#    q = np.gradient(temperature)*(gaps**3/12./mu)
#    del analyse_flux_case
#
#    return q,temperature,percolated

################################################################


def computeFluidFlux(step, myrun, bd_flag, do_not_use_akantu=False):

    mech_filename = f"./mechanics-step-{step}.plot.npz"
    fluid_filename = f"./fluid-step-{step}.plot.npz"
    surf_filename = "./surface.npz"
    cmd = (f'python analyse_flux_case.py --input {mech_filename} '
           f'--surface {surf_filename} --output {fluid_filename}')

    if do_not_use_akantu:
        cmd += " --do_not_use_akantu"
    print(cmd)
    ret = subprocess.call(cmd, shell=True)
    if ret:
        raise RuntimeError(f"Failed command: {cmd} {ret}")

    if bd_flag is False:
        return

    if os.path.exists("percolated"):
        print("Percolation was detected")
        return

    f = np.load(fluid_filename)
    q = f['q']
    trapped_area = f['trapped_area']
    conductive_area = f['conductive_area']

    qx = q[0]
    qy = q[1]
    qx[np.isnan(qx)] = 0.
    qy[np.isnan(qy)] = 0.

    qm_x = qx.sum()
    qm_y = qy.sum()
    q2_x = (qx**2).sum()
    q2_y = (qy**2).sum()
    qm = np.sqrt(q2_x+q2_y).sum()

    myrun.pushScalarQuantity(qm_x, step, "qm_x")
    myrun.pushScalarQuantity(qm_y, step, "qm_y")
    myrun.pushScalarQuantity(q2_x, step, "q2_x")
    myrun.pushScalarQuantity(q2_y, step, "q2_y")
    myrun.pushScalarQuantity(qm, step, "qm")
    myrun.pushScalarQuantity(trapped_area, step, "trapped_area")
    myrun.pushScalarQuantity(conductive_area, step, "conductive_area")

################################################################


def clusterInfo(myrun, traction, step, cluster_dump):

    # Cluster detection
    print("===============================")
    print("Detect contact clusters: ")
    clusters = tm.FloodFill.getClusters(traction > 0, False)
    print(f"Found {len(clusters)}")
    if (cluster_dump != ""):
        print("===============================")
        print("Output to disk")
        fname = cluster_dump + "step-" + step
        np.savez_compressed(fname, **{'clusters': clusters})
    print("===============================")
    print("Analyze contact clusters")
    cluster_areas = np.array([c.area for c in clusters])
    # holes_areas = []
    # for c in clusters:
    #     holes_areas += c.getHolesAreas()

    # myrun.pushVectorQuantity(
    #     holes_areas, step, "cluster_holes", is_integer=True)

    myrun.pushVectorQuantity(
        cluster_areas, step, "cluster_areas", is_integer=True)
    cluster_perimeters = np.array([c.perimeter for c in clusters])
    myrun.pushVectorQuantity(cluster_perimeters, step,
                             "cluster_perimeters", is_integer=True)

    myrun.pushScalarQuantity(np.array(cluster_areas).sum(
    ), step, "cluster_total_area")
    myrun.pushScalarQuantity(np.array(cluster_perimeters).sum(
    ), step, "cluster_total_perimeter")
    # myrun.pushScalarQuantity(clusters.getNbClustersWithHole(
    # ), step, "nb_clusters_with_hole")
    myrun.pushScalarQuantity(len(clusters),
                             step, "nb_clusters")

    if cluster_dump != "":
        print("\nOutput contact clusters")
        fname = cluster_dump + "step-" + step
        clusters.dumpAllClusters(fname)

    print("\n#=============================")

################################################################


def makePressures():
    study = "fluid"  # "mechanical"/"fluid"

    # Version 1 (mechanical contact): fine stepping for low pressures (~ linear regime) and coarse stepping up to full contact
    if study == "mechanical":
        half_load_steps = 60
        mid_pressure = 0.06
        max_pressure = 2.

        pressure_1 = np.arange(0., mid_pressure, mid_pressure/half_load_steps)
        pressure_2 = np.arange(mid_pressure, max_pressure,
                               (max_pressure-mid_pressure)/half_load_steps)
        pressures = np.append(pressure_1, pressure_2)
    # Version 2 (fluid problem): fine stepping for low pressures, coarse for average, and fine near the percolation
    # For the latter, the range of fine discretization is very broad (0.2 to 0.42) to capture the percolation
    # which occurs at p~0.25 for H=0.4 and high magnification and at p~0.4 for H=0.8 and k1=1
    # The first range corresponds to a nonlinear transition in permeability and it is good to capture.
    elif study == "fluid":
        load_steps_1 = 20
        load_steps_2 = 20
        load_steps_3 = 100
        min_pressure = 0.05
        mid_pressure = 0.2
        max_pressure = 0.42

        pressure_1 = np.arange(0., min_pressure, min_pressure/load_steps_1)
        pressure_2 = np.arange(min_pressure, mid_pressure,
                               (mid_pressure-min_pressure)/load_steps_2)
        pressure_3 = np.arange(mid_pressure, max_pressure,
                               (max_pressure-mid_pressure)/load_steps_3)
        pressures = np.concatenate((pressure_1, pressure_2, pressure_3))
    else:
        raise RuntimeError("Invalid study type.")

    # pressures = []
    # nsteps = 40
    # max_p = .3
    # step = max_p/float(nsteps)
    # for i in range(1,nsteps+1):
    #    if i <= 20: pressures.append(i*step)
    #    else:       pressures += [(i-0.5)*step,i*step]

    return pressures[1:]


################################################################
bin_sizes = []
bins = {}
distrib = {}
################################################################


def parseArguments(argv):
    parser = argparse.ArgumentParser(
        description='Pressure Study by Tamaas')

    parser.add_argument("--N", type=int,
                        help="Surface size", required=True)
    parser.add_argument("--Nmax", type=int,
                        help="Fine Surface size", required=True)
    parser.add_argument(
        "--desired_rms", type=float, required=True,
        help=("root mean square desired (either of heights or slopes) for "
              "the generated surface"))
    parser.add_argument("--hurst", type=float,
                        help="Hurst parameter of the surface", required=True)
    parser.add_argument("--k0", type=int,
                        help="low frequency rolloff cutoff of the surface",
                        required=True)
    parser.add_argument("--k1", type=int,
                        help="low frequency cutoff of the surface",
                        required=True)
    parser.add_argument("--k2", type=int,
                        help="high frequency cutoff of the surface",
                        required=True)
    parser.add_argument("--seed", type=int,
                        help="random seed used to generate the surface",
                        required=True)
    parser.add_argument(
        "--epsilon", type=float, required=True,
        help=("relative precision, convergence if "
              "| (f_i - f_{i-1})/f_i | < Precision."))
    parser.add_argument("--blackdynamite", type=str,
                        help="activate the blackdynamite output",
                        default="false")
    parser.add_argument(
        "--plot_surface", type=str, default="false",
        help=("request output of text files containing the "
              "contact pressures on the surface"))
    parser.add_argument(
        "--distribution_factor", type=int, required=True,
        help=("set the multiplication factor to be used on the max "
              "pressure to callibrate the pressure distribution range"))
    parser.add_argument("--e_star", type=float,
                        help="set the effective elastic modulus",
                        required=True)
    parser.add_argument("--L", type=float,
                        help="set the lateral size of the surface",
                        default=1.)
    parser.add_argument(
        "--max_search", type=int,
        help="set the maximal number of line search for the minization",
        default=100000)
    parser.add_argument("--height_distribution", type=str,
                        help="ask to compute the height distribution and quit",
                        default="false")
    parser.add_argument(
        "--constrain_heights", type=str,
        help="constrain the RMS of heights instead of the RMS of slopes",
        default='false')
    parser.add_argument("--nthreads", type=int,
                        help="number of threads to use for fourier transforms",
                        default=1)
    parser.add_argument("--cluster_file", type=str,
                        help="request generation of a file with cluster info",
                        default="")
    parser.add_argument(
        "--dump_freq", type=int, default="100",
        help="frequency at which you should output convergence report")
    parser.add_argument("--compute_flux", type=str,
                        help="flag to compute the fluid flux", default="False")
    parser.add_argument("--do_not_use_akantu", action="store_true",
                        help="Use this flag to de-activate akantu solver and use finite differential solver instead")

    args = parser.parse_args(argv[1:])
    return vars(args)


################################################################

def main(argv):

    arguments = parseArguments(argv)
    try:
        os.remove("percolated")
    except:
        pass

    do_not_use_akantu = arguments["do_not_use_akantu"]
    n = arguments["N"]
    fine_n = arguments["Nmax"]
    desired_rms = arguments["desired_rms"]
    hurst = arguments["hurst"]
    k0 = arguments["k0"]
    k1 = arguments["k1"]
    k2 = arguments["k2"]
    seed = arguments["seed"]
    epsilon = arguments["epsilon"]
    bd_flag = arguments["blackdynamite"].lower() == 'true'
    plot_surface = arguments["plot_surface"].lower() == 'true'
    distribution_factor = arguments["distribution_factor"]
    e_star = arguments["e_star"]
    # L = arguments["L"]
    # max_search = arguments["max_search"]
    height_distribution = arguments["height_distribution"].lower() == 'true'
    constrain_heights = arguments["constrain_heights"].lower() == 'true'
    compute_flux = arguments["compute_flux"].lower() == 'true'
    cluster_file = arguments["cluster_file"]
    # nthreads = arguments["nthreads"]
    # dump_freq = arguments["dump_freq"]

    pressures = makePressures()

    logger.debug("start logging")

    if bd_flag:
        logger.debug("Forging parser")
        myrun, _ = BD.getRunFromScript()
        myrun.start()
        logger.debug("run started")
    else:
        myrun = None

    a = np.log2(fine_n/n)
    if not int(a) == a:
        raise RuntimeError(f'invalid size inputs {fine_n} {n}')
    a = fine_n/n

    logger.debug("create surface objects")
    pressure = 0

    spectrum = tm.Isopowerlaw2D()
    spectrum.hurst = 0.8
    spectrum.q0 = k0
    spectrum.q1 = k1
    spectrum.q2 = k2

    generator = tm.SurfaceGeneratorFilter2D([fine_n, fine_n])
    generator.spectrum = spectrum
    generator.random_seed = seed
    surface = generator.buildSurface()
    s = surface / tm.Statistics2D.computeRMSHeights(surface)

    rms_slopes_spectral = stats.computeSpectralRMSSlope(s)
    rms_slopes_geometric = stats.computeFDRMSSlope(s)

    rms = stats.computeRMSHeights(s)

    if constrain_heights:
        s *= desired_rms/rms
    else:
        s *= desired_rms/rms_slopes_spectral

    rms_slopes_spectral = stats.computeSpectralRMSSlope(s)
    rms_slopes_geometric = stats.computeFDRMSSlope(s)
    rms = stats.computeRMSHeights(s)

    moments = stats.computeMoments(s)
    analytical_moments = spectrum.moments()

    m0 = moments[0]
    m2 = moments[1]
    m4 = moments[2]
    alpha = m0*m4/(m2*m2)

    analytic_m0 = analytical_moments[0]
    analytic_m2 = analytical_moments[1]
    analytic_m4 = analytical_moments[2]

    try:
        moment_A = m0/analytic_m0
    except RuntimeError:
        moment_A = 0.

    analytic_alpha = spectrum.alpha()
    height_bins, height_distrib = np.histogram(
        s, bins=1000, range=[-10, 10])
    max_heights = s.max()
    min_heights = s.min()

    max_pressure = e_star*rms_slopes_spectral

    if bd_flag:
        logger.debug("push surface stats")
        myrun.pushScalarQuantity(analytic_alpha, 0, "analytic_alpha")
        myrun.pushScalarQuantity(alpha, 0, "alpha")
        myrun.pushVectorQuantity(height_bins, 0, "height_bins")
        myrun.pushVectorQuantity(height_distrib, 0, "height_distrib")
        myrun.pushScalarQuantity(max_heights, 0, "max_heights")
        myrun.pushScalarQuantity(min_heights, 0, "min_heights")
        myrun.pushScalarQuantity(rms, 0, "rms_spectral")
        myrun.pushScalarQuantity(
            rms_slopes_spectral, 0, "rms_slopes_spectral")
        myrun.pushScalarQuantity(m0, 0, "m0")
        myrun.pushScalarQuantity(m2, 0, "m2")
        myrun.pushScalarQuantity(m4, 0, "m4")
        myrun.pushScalarQuantity(
            analytic_m0, 0, "analytic_m0")
        myrun.pushScalarQuantity(analytic_m2, 0, "analytic_m2")
        myrun.pushScalarQuantity(analytic_m4, 0, "analytic_m4")

    print("\n::: DATA :::::::::::::::::::::::::::::: ")
    print("   [N]                   {0}".format(n))
    print("   [rms]                 {0}".format(rms))
    print("   [rmsSlopeSpectral]    {0}".format(rms_slopes_spectral))
    print("   [rmsSlopeGeometric]   {0}".format(rms_slopes_geometric))
    print("   [Hurst]               {0}".format(hurst))
    print("   [k0]                  {0}".format(k0))
    print("   [k1]                  {0}".format(k1))
    print("   [k2]                  {0}".format(k2))
    print("   [moment A]            {0}".format(moment_A))
    print("   [m0]                  {0}".format(m0))
    print("   [analytic m0]         {0}".format(analytic_m0))
    print("   [m2]                  {0}".format(m2))
    print("   [analytic m2]         {0}".format(analytic_m2))
    print("   [m4]                  {0}".format(m4))
    print("   [analytic m4]         {0}".format(analytic_m4))
    print("   [alpha]               {0}".format(alpha))
    print("   [analytic_alpha]      {0}".format(analytic_alpha))
    print("   [seed]                {0}".format(seed))
    print("   [Precision]           {0}".format(epsilon))
    print("   [MaxPext]             {0}".format(max_pressure))
    print("   [height_distribution] {0}".format(height_distribution))

    if plot_surface:
        logger.debug("dump surface file")
        filename = "surface"
        np.savez_compressed(filename, **{'surface': s})

    if height_distribution:
        if bd_flag:
            myrun.finish()
        return

    logger.debug("create BEM objects")
    model = tm.ModelFactory.createModel(
        tm.model_type.basic_2d, [1., 1.], [n, n])

    model.E = e_star
    model.nu = 0
    s -= s.max()
    solver = tm.PolonskyKeerRey(model, s, epsilon)

    bin_sizes.append(100)
    bin_sizes.append(500)
    bin_sizes.append(1000)
    bin_sizes.append(2000)
    bin_sizes.append(5000)
    bin_sizes.append(10000)

    logger.debug("start main loop")

    for i in range(0, len(pressures)):
        pressure = pressures[i]*max_pressure
        logger.debug("compute equilibrium {0}".format(i))
        f = solver.solve(pressure)
        A = stats.contact(model.traction)
        V = stats.computeRMSHeights(model.traction)
        perimeter = sum(c.perimeter for c in tm.FloodFill.getClusters(
            model.traction > 0, False))
        pmean = np.average(model.traction[model.traction > 1e-10])

        print(
            f"* Inc : {i+1}/{len(pressures)}, "
            f"pressure = {pressure} area {A} standard deviation {V}")

        if bd_flag:
            logger.debug("push measures {0}".format(i))
            myrun.pushScalarQuantity(A, i, "area")
            myrun.pushScalarQuantity(V, i, "variance")
            myrun.pushScalarQuantity(pmean, i, "pmean")
            myrun.pushScalarQuantity(pressure, i, "pressure")
            myrun.pushScalarQuantity(perimeter, i, "perimeter")
            myrun.pushScalarQuantity(f, i, "f")
            logger.debug("compute cluster info {0}".format(i))
            clusterInfo(myrun, model.traction, i, cluster_file)
            # convergence_iterations = model.getConvergenceIterations()
            # myrun.pushVectorQuantity(
            #     convergence_iterations, i, "objective_function")
            myrun.base.commit()

        for ibins in range(0, len(bin_sizes)):
            logger.debug("compute pressure distribution {0}".format(i))
            bins[ibins], distrib[ibins] = np.histogram(model.traction, bin_sizes[ibins],
                                                       (0., max_pressure*distribution_factor))
            name_distrib = "pressureDistribution" + str(bin_sizes[ibins])
            name_bins = "pressureBins" + str(bin_sizes[ibins])

            if (bd_flag):
                logger.debug("push pressure distribution {0}".format(i))
                myrun.pushVectorQuantity(distrib[ibins], i, name_distrib)
                myrun.pushVectorQuantity(bins[ibins], i, name_bins)
                myrun.base.commit()

        if plot_surface:
            logger.debug("plot pressure surface {0}".format(i))
            filename = "mechanics-step-{0}.plot".format(i)
            # print(dir(model))
            to_output = {'trac': model.traction,
                         'gap': model.displacement-s
                         }
            np.savez_compressed(filename, **to_output)

        if compute_flux:
            computeFluidFlux(i, myrun, bd_flag,
                             do_not_use_akantu=do_not_use_akantu)
            if os.path.exists("percolated"):
                break
        if A == 1.0:
            break
    if bd_flag:
        logger.debug("changing state to finished")
        myrun['state'] = 'FINISHED'
        myrun.update()
        myrun.base.commit()
        myrun.finish()
        myrun.base.commit()

################################################################


if __name__ == '__main__':
    main(sys.argv)

################################################################
