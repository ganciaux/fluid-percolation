import numpy as np
import matplotlib.pyplot as plt
import sys

# Load data
if len(sys.argv) < 2:
    print("Usage: python plot_data_test.py <step>")
    sys.exit(1)
step = int(sys.argv[1])

file_solid = f"mechanics-step-{step}.plot.npz"
file_fluid = f"fluid-step-{step}.plot.npz"

data_solid = np.load(file_solid)
data_fluid = np.load(file_fluid)

# Show tags
print(data_solid.files)
print(data_fluid.files)

pressure = data_fluid['pressure']
flux = data_fluid['q']

# Plot pressure and flux
fig, ax = plt.subplots(1, 2, figsize=(10, 5))
cax = ax[0].imshow(pressure)
fig.colorbar(cax, label='Pressure')
ax[0].set_title('Pressure')

log_flux = np.log10(np.sqrt(flux[:, :, 0]**2 + flux[:, :, 1]**2))
log_flux[log_flux == -np.inf] = np.nan
min_flux = np.nanmin(log_flux)
max_flux = np.nanmax(log_flux)
mean_flux = (min_flux + max_flux) / 2
delta = (max_flux - min_flux) / 2

cax = ax[1].imshow(log_flux, vmin=mean_flux +
                   delta/1.5, vmax=mean_flux + delta)
fig.colorbar(cax, label='Log10(Flux Magnitude)')
ax[1].set_title('Flux')
plt.show()
