set -x 

echo $HOSTNAME
echo $PATH
export HOST=__BLACKDYNAMITE__dbhost__
export SCHEMA=__BLACKDYNAMITE__study__
export RUN_ID=__BLACKDYNAMITE__run_id__

export N=__BLACKDYNAMITE__n__
export Nmax=__BLACKDYNAMITE__nmax__
export rms=__BLACKDYNAMITE__desired_rms__
export hurst=__BLACKDYNAMITE__hurst__
export E=__BLACKDYNAMITE__e__
export k0=__BLACKDYNAMITE__k0__
export k1=__BLACKDYNAMITE__k1__
export k2=__BLACKDYNAMITE__k2__
export seed=__BLACKDYNAMITE__seed__
export precision=__BLACKDYNAMITE__precision__
export distribution_factor=__BLACKDYNAMITE__distribution_factor__
export height_distribution=__BLACKDYNAMITE__height_distribution__
export plot_surface=__BLACKDYNAMITE__plot_surface__
export constrain_heights=__BLACKDYNAMITE__constrain_heights__
export compute_flux=__BLACKDYNAMITE__compute_flux__
export nproc=__BLACKDYNAMITE__nproc__

source /scratch/anciaux/fluid-percolation/python-env/bin/activate

python discretization_study.py --e_star $E --N $N --desired_rms $rms --hurst $hurst --k0 $k0 --k1 $k1 --k2 $k2 --seed $seed --epsilon $precision --blackdynamite True --distribution_factor $distribution_factor --height_distribution $height_distribution --plot_surface $plot_surface --constrain_heights $constrain_heights --nthreads $nproc --compute_flux $compute_flux --Nmax $Nmax --do_not_use_akantu
if [ $? != 0 ]; then
canYouDigIt runs update --truerun "state=BEM FAILED"
fi
