#!/usr/bin/python3.10
################################################################
import matplotlib.pyplot as plt
import numpy as np
import tag_island
import akantu
import os
import sys
path = '~/repositories/akantu/build/python'
path = os.path.expanduser(path)
sys.path.append(path)
path = '~/repositories/createRoughSurface/pressure_study/build'
path = os.path.expanduser(path)
sys.path.append(path)

sys.path.append('./build')
################################################################


def plotSurface(surf):
    fig = plt.figure()
    axe = fig.add_subplot(111)
    img = axe.imshow(surf)
    cbar = fig.colorbar(img)

################################################################


def setupDump(model, trac, disp, gaps, surf, contact, tag_map, count_map):
    model.setBaseName("heat_transfer_square2d")
    model.setTextModeToDumper()
    model.addDumpField("temperature")
    model.addDumpField("residual")
    model.addDumpField("blocked_dofs")
    model.addDumpField("conductivity")
    model.addDumpField("temperature_gradient")
    mesh = model.getMesh()
    nb_nodes = mesh.getNbNodes()
    mesh.addDumpFieldExternalReal("gaps", gaps.reshape((nb_nodes, 1)))
    mesh.addDumpFieldExternalReal("trac", trac.reshape((nb_nodes, 1)))
    mesh.addDumpFieldExternalReal("disp", disp.reshape((nb_nodes, 1)))
    mesh.addDumpFieldExternalReal("surf", surf.reshape((nb_nodes, 1)))
    mesh.addDumpFieldExternalReal("contact", contact.reshape((nb_nodes, 1)))
    mesh.addDumpFieldExternalReal("tag_map", tag_map.reshape((nb_nodes, 1)))
    mesh.addDumpFieldExternalReal(
        "count_map", count_map.reshape((nb_nodes, 1)))

################################################################


def findStart(tag_map, contact):
    res = -1
    n = tag_map.shape[0]
    for i in range(0, n):
        if (tag_map[i, 0] == -1 and contact[i, 0] == 0.):
            res = i
            break

    if (res == -1):
        return None
    # print("found starting point " + str(res) + " " + str(tag_map[res, 0]))
    return res, 0

################################################################


def tagIsland(island_number, start, contact, boundary, mesh, index_range, cond_on_quads, contact_on_quads, tag_map, node2element):
    n = contact.shape[0]
    current = start
    exploration = []
    jmax = 0

    while (1):

        i, j = current

        jmax = max(jmax, j)

        if contact[current] == 0:
            tag_map[current] = island_number
            if i == 0:
                tag_map[n-1, j] = island_number

        def testNeigh(i, j, ii, jj):

            if jj == -1:
                return
            if jj == n:
                return

            ii = ii % (n-1)
            i = i % (n-1)

            if (tag_map[ii, jj] == -1. and contact[ii, jj] == 0.):

                def getConnectedElems(iii, jjj):
                    it = node2element.row(index_range[iii, jjj])
                    elems = set([e.element for e in it])
                    return elems

                elems1 = getConnectedElems(i, j)
                elems2 = getConnectedElems(ii, jj)

                if i == 0:
                    elems1 = elems1.union(getConnectedElems(n-1, j))
                if ii == 0:
                    elems2 = elems2.union(getConnectedElems(n-1, jj))

                elems = elems1.intersection(elems2)
                if len(elems) == 0:
                    return
                cond = [contact_on_quads[el] >
                        0. or cond_on_quads[el] == 0. for el in elems]
                flag = True
                for c in cond:
                    flag &= c
                if flag:
                    return
                exploration.append((ii, jj))

        testNeigh(i, j, i+1, j)
        testNeigh(i, j, i-1, j)
        testNeigh(i, j, i+1, j+1)
        testNeigh(i, j, i-1, j+1)
        testNeigh(i, j, i+1, j-1)
        testNeigh(i, j, i-1, j-1)
        testNeigh(i, j, i, j-1)
        testNeigh(i, j, i, j+1)

        if len(exploration) == 0:
            break
        current = exploration.pop()

    if jmax == n-1:
        return True
    return False

################################################################


def tagIslands(contact, boundary, mesh, index_range, cond_on_quads, contact_on_quads):
    tag_map = np.zeros(contact.shape)
    tag_map[:, :] = -1.

    node2element = akantu.CSRElement()
    akantu.MeshUtils.buildNode2Elements(mesh, node2element, 2)
    island_number = 0

    list_percolated = []
    while (1):

        start = findStart(tag_map, contact)
        if start == None:
            break
        percolated = tagIsland(island_number, start, contact, boundary, mesh,
                               index_range, cond_on_quads, contact_on_quads, tag_map, node2element)
        if percolated:
            list_percolated.append(island_number)
        print("island {0} percolated ? : {1}".format(
            island_number, percolated))
        island_number += 1

    # plotSurface(tag_map)
    for i in range(0, island_number):
        if i in list_percolated:
            tag_map[tag_map == i] = 0.
        else:
            print("doing " + str(i))
            tag_map[tag_map == i] = 1.

    none_percolated = len(list_percolated) == 0

    tag_map[tag_map == -1] = 1.
    # plotSurface(tag_map)
    # plt.show()
    print("Done coloring")
    return tag_map, none_percolated

################################################################


def rectifyConductivity(contact_on_quads, cond_on_quads, con, reverse_index, count_map):
    for el in range(0, contact_on_quads.shape[0]):
        if contact_on_quads[el] > 0.:
            cond_on_quads[el] = 0.
        if cond_on_quads[el] == 0.:
            for node in con[el]:
                i, j = reverse_index[node]
                count_map[i, j] += 1

################################################################


def renderPBC4FEM(mat):
    n = mat.shape[0]
    tmp = np.zeros((n+1, n+1))
    tmp[:n, :n] = mat[:, :]
    tmp[-1, :n] = mat[0, :]
    tmp[:n, -1] = mat[:, 0]
    tmp[n, n] = mat[0, 0]
    return np.copy(tmp)

################################################################


def computeFlux(trac, disp, surf, mu=1.):

    n = trac.shape[0]

    trac = renderPBC4FEM(trac)
    disp = renderPBC4FEM(disp)
    surf = renderPBC4FEM(surf)

    gaps = disp - surf - disp.max() + surf.max()
    # gaps[gaps<0.] = 0.
    gaps[trac > 0.] = 0.

    cond = np.copy(gaps**3/12./mu)
    spatial_dimension = 2

    print('create material.dat')
    fout = open('material.dat', 'wb')
    fout.write("""model heat_transfer_model [
      constitutive_law heat_diffusion [
      name = dummy
      density = 8940
     capacity = 385
     conductivity = [[401,   0,   0],\\
                     [0,   401,   0],\\
		     [0,     0, 401]]
]
]
    """.encode())
    fout.close()

    akantu.parseInput("material.dat")
    print('create mesh')
    # create mesh
    mesh = akantu.Mesh(spatial_dimension)
    meshAcc = akantu.MeshAccessor(mesh)

    # mesh.read('square_tri3.msh')
    x = np.linspace(0, 1., n+1)
    X, Y = np.meshgrid(x, x)

    index_range = np.array([i for i in range(0, (n+1)**2)])
    index_range = index_range.reshape((n+1, n+1))

#    print index_range
    nb_nodes = (n+1)**2
    nb_elements = 2*n**2
    meshAcc.resizeNodes(nb_nodes)
    meshAcc.resizeConnectivity(nb_elements, akantu._triangle_3)
    con = mesh.getConnectivity(akantu._triangle_3)
    connectivities = con.reshape((n, n, 2, 3))

    for i in range(0, n):
        for j in range(0, n):

            connectivities[i, j, 0, 0] = index_range[i+1, j]
            connectivities[i, j, 0, 2] = index_range[i+1, j+1]
            connectivities[i, j, 0, 1] = index_range[i, j+1]

            connectivities[i, j, 1, 0] = index_range[i, j+1]
            connectivities[i, j, 1, 2] = index_range[i, j]
            connectivities[i, j, 1, 1] = index_range[i+1, j]

    meshAcc.makeReady()
    # print(con)

    reverse_index = np.zeros((nb_nodes, 2), dtype='uint32')

    for i in range(0, n+1):
        for j in range(0, n+1):

            reverse_index[index_range[i, j], 0] = i
            reverse_index[index_range[i, j], 1] = j

    #    print con
    nodes = mesh.getNodes()
    # print nodes.shape
    nodes[:, 0] = X.reshape((n+1)**2)
    nodes[:, 1] = Y.reshape((n+1)**2)

    print('create heat transfer model')
    mesh.makePeriodic(akantu._y)
    model = akantu.HeatTransferModel(mesh)
    # initialize everything
    print('set boundary conditions')
    model.initFull(_analysis_method=akantu._static)
    # model.getSynchronizerRegistry().synchronize(akantu._gst_htm_temperature)
    # boundary conditions
    nodes = mesh.getNodes()
    boundary = model.getBlockedDOFs()
    temperature = model.getTemperature()
    nb_nodes = mesh.getNbNodes()
    temperature[:] = 1.
    temperature[nodes[:, 0] < 1./n/2.] = 1.
    boundary[nodes[:, 0] < 1./n/2.] = True
    temperature[nodes[:, 0] > 1. - 1./n/2.] = 0.
    boundary[nodes[:, 0] > 1. - 1./n/2.] = True

    #    for i in range (0, nb_nodes):
    #        temperature[i] = 1.;
    #        px = nodes[i,0];
    #        py = nodes[i,1];
    #        if px < 1./n/2.:
    #            boundary[i] = True;
    #            temperature[i] = 1.;
    #        if px > 1.-1./n/2.:
    #            boundary[i] = True;
    #            temperature[i] = 0.;
    #

    print('compute conductivity on quadrature points')
    #    temperature[:] = gaps.reshape((nb_nodes,1))[:]
    k_on_qpoints = model.getConstitutiveLaw(
        0).getInternalReal("diffusivity")(akantu._triangle_3)
    # print(k_on_qpoints)
    # k_on_qpoints = model.getConductivityOnQpoints(akantu._triangle_3)
    cond_on_quads = np.zeros((nb_elements, 1))

    cond = np.copy(cond)
    model.getFEEngine().interpolateOnIntegrationPoints(cond.reshape((nb_nodes, 1)),
                                                       cond_on_quads,
                                                       1,
                                                       akantu._triangle_3)

    contact_flag = trac > 0
    contact = np.zeros(trac.shape)
    contact[contact_flag] = 1.

    print('compute contact flag on quadrature points')
    contact_on_quads = np.zeros((nb_elements, 1))
    model.getFEEngine().interpolateOnIntegrationPoints(contact.reshape((nb_nodes, 1)),
                                                       contact_on_quads,
                                                       1,
                                                       akantu._triangle_3)

    #    plotSurface(contact)
    #    plotSurface(boundary)
    print('coloring of the islands')
    #    print type(index_range)
    #    print index_range.dtype
    #    print index_range[:16,:16]
    tag_map = tag_island.tag_islands(contact.reshape((n+1, n+1)), boundary.reshape(
        (n+1, n+1)), mesh, index_range, cond_on_quads, contact_on_quads)
    percolated = tag_island.is_percolated()
    # tag_map,percolated = tagIslands(contact,boundary,mesh,index_range,cond_on_quads,contact_on_quads)

    if percolated:
        print('percolated: stop here')
        temperature = np.copy(temperature.reshape((n+1, n+1)))
        temperature[:, :] = np.nan
        return temperature, True

    print('rectify the blocked nodes')

    # print type(tag_map)
    boundary[tag_map.reshape(boundary.shape) == 1.] = True
    temperature[tag_map.reshape(temperature.shape) == 1.] = -1
    count_map = np.zeros(contact.shape)

    print('rectify the conductivities')
    # rectifyConductivity(contact_on_quads,cond_on_quads, con, reverse_index, count_map)
    tag_island.rectify_conductivity(
        contact_on_quads, cond_on_quads, con, reverse_index, count_map)
    # for el in range(0,contact_on_quads.shape[0]):
    #   if contact_on_quads[el] > 0. : cond_on_quads[el] = 0.
    #   if cond_on_quads[el] == 0.:
    #     for node in con[el]:
    #       i,j = reverse_index[node]
    #       count_map[i,j] +=1

    print('rectify the blocked nodes once more')
    boundary[count_map.reshape(boundary.shape) == 6.] = True
    temperature[count_map.reshape(boundary.shape) == 6.] = -1

    # plotSurface(tag_map)
    # plotSurface(boundary.reshape((n+1,n+1)))
    # plt.show()

    print('compute the tensor conductivity')
    k_on_qpoints[:, 0] = cond_on_quads[:, 0]
    k_on_qpoints[:, 3] = cond_on_quads[:, 0]
    k_on_qpoints[:, 1] = 0.
    k_on_qpoints[:, 2] = 0.

    print('assemble conductivity matrix')
    model.assembleDiffisivityMatrix()
    print('compute residual')
    # model.assembleInternalFlow()

    # print 'setup dumper'
    # setupDump(model,trac,disp,gaps,surf,contact,tag_map,count_map)
    # print 'synch temperature'
    # model.getSynchronizerRegistry().synchronize(akantu._gst_htm_temperature)
    # print 'dump'
    # model.dump();
    print('solve')
    model.solveStep()
    # print('compute residual')
    # model.assembleInternalFlow()
    # print('synch temperature')
    # model.getSynchronizerRegistry().synchronize(akantu._gst_htm_temperature)
    # print 'dump'
    # model.dump()
    temperature = np.copy(temperature.reshape((n+1, n+1)))
    # akantu.finalize();
    return temperature, False

################################################################


def main():

    N = 256
    SG = SurfaceGeneratorFilterFFT()
    SG.getGridSize().assign(N)
    SG.getHurst().assign(0.8)
    SG.getRMS().assign(1.)
    SG.getQ0().assign(4)
    SG.getQ1().assign(4)
    SG.getQ2().assign(64)
    SG.getRandomSeed().assign(20)
    SG.Init()
    s = SG.buildSurface()
    rms_slopes_spectral = SurfaceStatistics.computeSpectralRMSSlope(s)
    s.rescaleHeights(1./rms_slopes_spectral)
    surf = s.getWrappedNumpy().real
    surf -= surf.max()

    bemKato = BemKato(s)
    bemKato.setEffectiveModulus(1.)
    pressure = .1
    bemKato.computeEquilibrium(1e-14, pressure, 1)
    trac = bemKato.getTractions().getWrappedNumpy().real
    disp = bemKato.getDisplacements().getWrappedNumpy().real

    maps = {}
    maps['trac'] = trac
    maps['disp'] = disp
    maps['surf'] = surf
    np.savez_compressed('case-{0}'.format(N), **maps)

    computeFlux(trac, disp, surf)


if __name__ == "__main__":

    import os
    import sys
    path = '~/repositories/createRoughSurface/build/'
    path = os.path.expanduser(path)
    sys.path.append(path)

    from python_surface import *

    main()
