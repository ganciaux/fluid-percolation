import numpy as np
import akantu
from collections import deque

# Global variables
global percolated
percolated = False
tag_map = None


def is_percolated():
    return percolated


Coords = tuple


def find_start(tag_map, contact):
    res = -1
    n = tag_map.shape[0]
    for i in range(n):
        if tag_map[i, 0] == -1 and contact[i, 0] == 0:
            res = i
            break
    if res == -1:
        return Coords((-1, -1))
    # print(f"found starting point {res} {tag_map[res, 0]}")
    return Coords((res, 0))


def get_connected_elems(iii, jjj, mesh, index_range):
    elems = set()
    for el in mesh.getConnectedElementToNode(index_range[iii, jjj]):
        elems.add(el)
    return elems


def _test_neigh(i, j, ii, jj, n, tag_map, contact, mesh,
                index_range, contact_on_quads, cond_on_quads, exploration):
    if jj == -1 or jj == n:
        return

    ii = ii % (n - 1)
    i = i % (n - 1)

    if tag_map[ii, jj] == -1 and contact[ii, jj] == 0:
        elems1 = get_connected_elems(i, j, mesh, index_range)
        elems2 = get_connected_elems(ii, jj, mesh, index_range)

        if i == 0:
            elems1.update(get_connected_elems(
                n - 1, j, mesh, index_range))
        if ii == 0:
            elems2.update(get_connected_elems(
                n - 1, jj, mesh, index_range))

        elems = elems1.intersection(elems2)
        if not elems:
            return

        flag = all(contact_on_quads[el] >
                   0 or cond_on_quads[el] == 0 for el in elems)
        if flag:
            return

        exploration.append((ii, jj))


def tag_island(island_number,
               start,
               contact,
               boundary,
               mesh,
               index_range,
               cond_on_quads,
               contact_on_quads,
               tag_map,
               node2element):
    n = contact.shape[0]
    current = start
    exploration = deque()
    jmax = 0

    while True:
        i, j = current

        jmax = max(jmax, j)

        if contact[i, j] == 0:
            tag_map[i, j] = island_number
            if i == 0:
                tag_map[n - 1, j] = island_number

        _test_neigh(i, j, i + 1, j, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i - 1, j, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i + 1, j + 1, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i - 1, j + 1, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i + 1, j - 1, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i - 1, j - 1, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i, j - 1, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)
        _test_neigh(i, j, i, j + 1, n, tag_map, contact, mesh,
                    index_range, contact_on_quads, cond_on_quads, exploration)

        if not exploration:
            break
        current = exploration.pop()

    return jmax == n - 1


def tag_islands(contact, boundary, mesh, index_range, cond_on_quads, contact_on_quads):
    global tag_map
    if tag_map is None:
        tag_map = np.full_like(contact, -1.0)

    island_number = 0
    list_percolated = set()

    while True:
        start = find_start(tag_map, contact)
        if start == Coords((-1, -1)):
            break
        percolated = tag_island(island_number, start, contact, boundary, mesh,
                                index_range, cond_on_quads, contact_on_quads, tag_map, mesh)
        if percolated:
            list_percolated.add(island_number)
        island_number += 1

    none_percolated = not list_percolated

    n = tag_map.shape[0]
    for i in range(island_number):
        if i in list_percolated:
            tag_map[tag_map == i] = 0.0
        else:
            tag_map[tag_map == i] = 1.0

    tag_map[tag_map == -1] = 1.0

    percolated = none_percolated
    return tag_map


def rectify_conductivity(contact_on_quads, cond_on_quads, con, reverse_index, count_map):
    nb_elem = contact_on_quads.size
    nb_node = contact_on_quads.shape[1]

    for el in range(nb_elem):
        if contact_on_quads[el] > 0:
            cond_on_quads[el] = 0
        if cond_on_quads[el] == 0:
            for nd in range(nb_node):
                i = reverse_index[nd, 0]
                j = reverse_index[nd, 1]
                count_map[i, j] += 1
