import numpy as np
import fluid_solver as FS


def renderPBC4FEM(mat):
    n = mat.shape[0]
    tmp = np.zeros((n+1, n+1))
    tmp[:n, :n] = mat[:, :]
    tmp[-1, :n] = mat[0, :]
    tmp[:n, -1] = mat[:, 0]
    tmp[n, n] = mat[0, 0]
    return np.copy(tmp)


def computeFlux(trac, disp, surf, mu=1.):
    # this steps created an additional row and column for proper meshing
    # this was done for the FEM solver, but I guess you also need it for the
    # finite difference solver as well

    trac = renderPBC4FEM(trac)
    disp = renderPBC4FEM(disp)
    surf = renderPBC4FEM(surf)

    # This is the calculation of the gap with respect to
    # surface, and displacements
    gaps = disp - surf - disp.max() + surf.max()

    # zeroing gaps when traction is positive to avoid round-off errors
    gaps[trac > 0.] = 0.

    # solver_type = "direct"
    solver_type = "iterative"
    percolated, _, pressure, flux, trapped_area, conductive_area = FS.solve_fluid_problem(
        gaps, solver_type)

    return pressure, percolated, gaps, flux, trapped_area, conductive_area
