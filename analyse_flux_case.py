#!/usr/bin/env python

import finite_diff_solver
import gzip
import argparse
import sys
import os
import numpy as np
################################################################


def loadMap(filename):
    print('load {0}'.format(filename))
    f = gzip.open(filename)
    lines = []
    for _l in f:
        e = _l.strip().split()
        if not len(e) == 3:
            continue
        lines.append((int(e[0]), int(e[1]), float(e[2])))

    if len(lines) == 0:
        return None
    lines = np.array(lines)
    m = int(lines[:, 0].max())+1
    n = int(lines[:, 1].max())+1

    if not m == n:
        raise Exception('Not a square matrix: {0} x {1}'.format(m, n))
    out = np.zeros((m, n))
    for i, j, v in lines:
        out[i, j] = v
    return out

################################################################


def formatCaseName(filename):
    basename = os.path.basename(filename)
    dirname = os.path.dirname(filename)
    basename = "-".join(basename.split('-')[1:])
    surfname = "-".join(basename.split('-')[:16]) + ".plot.gz"
    return basename, dirname, surfname

################################################################


def loadMaps(filename, surface=None):

    basename, dirname, surfname = formatCaseName(filename)

    maps = {}

    if surface is None:
        maps['surf'] = loadMap(os.path.join(dirname, surfname))
    else:
        maps['surf'] = surface

    maps['surf'] -= maps['surf'].max()

    for h in ['gap', 'pressure']:
        filename = os.path.join(dirname, h+'-'+basename)
        maps[h] = loadMap(filename)

    maps['trac'] = maps['pressure']

    disp = maps['gap'] + maps['surf']
    disp -= disp.max()
    maps['disp'] = disp

    for k in maps.keys():
        if maps[k] is None:
            return None

    return maps
################################################################


def formatNPZCaseName(filename):
    basename = os.path.basename(filename)
    dirname = os.path.dirname(filename)
    surfname = "-".join(basename.split('-')[:16]) + ".npz"
    print(basename, dirname, surfname)
    return dirname, surfname

################################################################


def myLoad(fname, surfname=None, surface=None):

    try:
        maps = dict(np.load(fname))
        globals().update(maps)

        if surface is None:
            if surfname is None:
                dirname, surfname = formatNPZCaseName(fname)
                surfname = os.path.join(dirname, surfname)
            maps['surf'] = np.load(surfname)['surface']
        else:
            maps['surf'] = surface

        if 'disp' not in maps:
            disp = maps['gap'] + maps['surf']
            disp -= disp.max()
            maps['disp'] = disp

    except Exception as e:
        raise e
        print(e)
        maps = loadMaps(fname, surface)
        if maps is not None:
            globals().update(maps)

    if maps is not None:
        return dict(maps)
    else:
        return maps


################################################################
def main():

    parser = argparse.ArgumentParser(
        description='Analyze flux through contact clusters')

    parser.add_argument("--input", type=str,
                        help="NPZ file containing results from mechanics", required=True)
    parser.add_argument("--do_not_use_akantu", action="store_true",
                        help="Use this flag to de-activate akantu solver and use finite differential solver instead")
    parser.add_argument("--surface", type=str,
                        help="NPZ file containing surface", default=None)
    parser.add_argument("--output", type=str,
                        help="NPZ file where to store flux results", required=True)
    parser.add_argument("--plot",
                        help="Flag to plot result", action='store_true')

    args = vars(parser.parse_args(sys.argv[1:]))
    fname = args['input']
    surfname = args['surface']
    output = args['output']
    plot_flag = args['plot']

    maps = myLoad(fname, surfname=surfname)
    print(f"In {fname}, found: {[e for e in maps.keys()]}")
    trac = maps["trac"]
    disp = maps["disp"]
    surf = maps["surf"]
    globals().update(maps)

    mu = 1.

    if args['do_not_use_akantu']:
        pressure, percolated, gaps, q, trapped_area, conductive_area = finite_diff_solver.computeFlux(
            trac, disp, surf)

        if not percolated:
            maps['trapped_area'] = trapped_area
            maps['conductive_area'] = conductive_area
    else:
        import heat_akantu
        pressure, percolated = heat_akantu.computeFlux(trac, disp, surf)
        pressure[pressure == -1] = np.nan

        gaps = disp - surf - disp.max() + surf.max()
        gaps = heat_akantu.renderPBC4FEM(gaps)

    if not percolated:
        maps['pressure'] = pressure
        maps['q'] = q/(12*mu)  # np.gradient(temperature)*(gaps**3/12./mu)
        maps['percolated'] = percolated

    else:
        print("Percolation detected. Stop fluid simulations.")
        with open("percolated", "w") as f:
            f.write("Percolation was detected")
        return

    #    basename,dirname,surfname = formatCaseName(fname)
    #    basename = os.path.splitext(basename)[0]
    np.savez_compressed(output, **maps)
    if plot_flag:
        import matplotlib.pyplot as plt
        plt.imshow(pressure)
        plt.show()

################################################################


if __name__ == "__main__":
    main()
