# Installation

```
pip install -r requirements.txt
```

# Instruction for Vlad: how to start

a ready to test case can be launched with

```
bash test.sh
```

It will fail as you have to edit the file `finite_diff_solver.py` with the routine for you to populate with code.


## Issues and resolved issues

#### Open

+ We store `gap` and `trac` twice in `mechanics-step` and `fluid-step`

#### Resolved

+ VY added different pressure distribution to capture the "mise en contact" and better resolve the percolation.
+ In flux function, I do not need `trac: numpy of tractions, disp: numpy of displacements` but rather a gap field `gap`. @anciaux will you adjust it or I can do it?
+ I added the code to extract what you need => https://gitlab.com/ganciaux/fluid-percolation/-/blob/main/finite_diff_solver.py?ref_type=heads#L25
+ By default I have python3.12 and tamaas is not compatible with this version, at least pip3.12 cannot install it.
+ I switched to python3.10 and replaced (temporary) /usr/bin/python by /usr/bin/python3.10 (sorry, I have no rights to access `update-alternatives`)

## Status

+ Aug 28: operational fluid solver.

+ Jun 19: current status @vyastreb: working on the finite difference solver, have some troubles in elimination of non conducting islands.

+ I had to do this step when using Akantu. can't you reuse that part ? 
  please have a look at code at https://gitlab.com/ganciaux/fluid-percolation/-/blob/main/heat_akantu.py?ref_type=heads#L346 and https://gitlab.com/ganciaux/fluid-percolation/-/blob/main/heat_akantu.py?ref_type=heads#L366
  
  
